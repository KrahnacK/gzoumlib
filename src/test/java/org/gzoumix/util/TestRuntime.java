package org.gzoumix.util;

import org.gzoumix.util.agent.Runtime;
import org.gzoumix.util.agent.runtime.RuntimeThread;
import org.gzoumix.util.agent.task.ITask;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;

public class TestRuntime {

  private static final int NB_EXECUTIONS  = 2;
  private static final int NB_MESSAGES = 5;


  public static class Ping implements ITask {

    private final Collection<ITask> next;
    private final Object[] guard;
    private Pong pong;
    private Runtime runtime;
    public int nbExecutions;

    public Ping(Runtime runtime) {
      this.runtime = runtime;
      this.nbExecutions = 0;
      this.next = new ArrayList<>();
      this.next.add(this);

      this.guard = new Object[1];
      this.guard[0] = null; // Ping starts
    }

    public void setPong(Pong pong) { this.pong = pong; }

    @Override
    public Collection<ITask> run() {
      System.out.println("++Ping");
      runtime.log(Log.LogLevel.INFO, "++Ping");
      this.nbExecutions++;
      this.guard[0] = this.pong; // need to do that every run, as it is set to null when the guard is validated
      runtime.signal(this);
      if (this.nbExecutions < NB_EXECUTIONS) {
        return this.next;
      } else {
        return null;
      }
    }

    @Override
    public Object[] getGuard() {
      return this.guard;
    }

    @Override
    public Object getGroup() {
      return this;
    }
  }

  public static class Pong implements ITask {

    private final Collection<ITask> next;
    private final Object[] guard;
    private Ping ping;
    private Runtime runtime;
    public int nbExecutions;

    public Pong(Runtime runtime) {
      this.runtime = runtime;
      this.nbExecutions = 0;
      this.next = new ArrayList<>();
      this.next.add(this);

      this.guard = new Object[1];
      this.guard[0] = this.ping;
    }

    public void setPing(Ping ping) { this.ping = ping; this.guard[0] = this.ping; }

    @Override
    public Collection<ITask> run() {
      System.out.println("--Pong");
      runtime.log(Log.LogLevel.INFO, "--Pong");
      this.nbExecutions++;
      this.guard[0] = this.ping;
      runtime.signal(this);
      if (this.nbExecutions < NB_EXECUTIONS) {
        return this.next;
      } else {
        return null;
      }
    }

    @Override
    public Object[] getGuard() {
      return this.guard;
    }

    @Override
    public Object getGroup() {
      return this;
    }
  }


  @Test
  public void testPingPong() throws Exception {
    Runtime runtime = new Runtime();
    runtime.logger.setLogLevel(Log.LogLevel.DEBUG);
    Ping ping = new Ping(runtime);
    Pong pong = new Pong(runtime);

    ping.setPong(pong);
    pong.setPing(ping);

    RuntimeThread t1 = new RuntimeThread(runtime, NB_MESSAGES, 1);
    RuntimeThread t2 = new RuntimeThread(runtime, NB_MESSAGES, 2);
    runtime.add(t1);
    runtime.add(t2);

    runtime.task(ping);
    runtime.task(pong);

    runtime.start();

    //Thread.sleep(5000); //too ugly
    runtime.stop();
    runtime.logger.toStream(System.out);
    Assert.assertEquals(NB_EXECUTIONS, ping.nbExecutions);
    Assert.assertEquals(NB_EXECUTIONS, pong.nbExecutions);
    Assert.assertEquals(false, runtime.logger.hasError());
  }
}

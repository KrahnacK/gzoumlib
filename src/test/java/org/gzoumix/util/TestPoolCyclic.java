package org.gzoumix.util;

import org.gzoumix.util.agent.data.PoolCyclic;
import org.gzoumix.util.agent.data.PoolCyclicExtensible;
import org.junit.Assert;
import org.junit.Test;

public class TestPoolCyclic {

  private static final int CAPACITY = 5;

  private static final String MESSAGE_1 = "m1";
  private static final String MESSAGE_2 = "m2";
  private static final String MESSAGE_3 = "m3";
  private static final String MESSAGE_4 = "m4";
  private static final String MESSAGE_5 = "m5";
  private static final String MESSAGE_6 = "m6";
  private static final String MESSAGE_7 = "m7";

  @Test
  public void testPoolCyclic() throws Exception {
    PoolCyclic<String> pool = new PoolCyclic<>(CAPACITY);
    String m;

    // Test 1
    if(!pool.offer(MESSAGE_1)) { Assert.fail("PoolCyclic: error in putting one element"); }
    m = pool.pool();
    Assert.assertSame("PoolCyclic: error in putting/retrieving one element", m, MESSAGE_1);

    // Test 2
    if(!pool.offer(MESSAGE_1)) { Assert.fail("PoolCyclic: error in putting 1 element"); }
    if(!pool.offer(MESSAGE_2)) { Assert.fail("PoolCyclic: error in putting 2 element"); }
    if(!pool.offer(MESSAGE_3)) { Assert.fail("PoolCyclic: error in putting 3 element"); }
    if(!pool.offer(MESSAGE_4)) { Assert.fail("PoolCyclic: error in putting 4 element"); }
    if(!pool.offer(MESSAGE_5)) { Assert.fail("PoolCyclic: error in putting 5 element"); }
    if(!pool.isFull()) { Assert.fail("PoolCyclic: pool should be full"); }

    m = pool.pool();
    Assert.assertSame("PoolCyclic: error in putting/retrieving 5 elements (1)", m, MESSAGE_1);
    m = pool.pool();
    Assert.assertSame("PoolCyclic: error in putting/retrieving 5 elements (2)", m, MESSAGE_2);
    m = pool.pool();
    Assert.assertSame("PoolCyclic: error in putting/retrieving 5 elements (3)", m, MESSAGE_3);
    m = pool.pool();
    Assert.assertSame("PoolCyclic: error in putting/retrieving 5 elements (4)", m, MESSAGE_4);
    m = pool.pool();
    Assert.assertSame("PoolCyclic: error in putting/retrieving 5 elements (5)", m, MESSAGE_5);

    if(!pool.isEmpty()) { Assert.fail("PoolCyclic should be empty"); }
  }

  @Test
  public void testPoolCyclicExtensible() throws Exception {
    PoolCyclicExtensible<String> pool = new PoolCyclicExtensible<>(CAPACITY);
    String m;

    if(!pool.offer(MESSAGE_1)) { Assert.fail("PoolCyclicExtensible: error in putting 1 element"); }
    if(!pool.offer(MESSAGE_2)) { Assert.fail("PoolCyclicExtensible: error in putting 2 element"); }
    if(!pool.offer(MESSAGE_3)) { Assert.fail("PoolCyclicExtensible: error in putting 3 element"); }
    if(!pool.offer(MESSAGE_4)) { Assert.fail("PoolCyclicExtensible: error in putting 4 element"); }
    if(!pool.offer(MESSAGE_5)) { Assert.fail("PoolCyclicExtensible: error in putting 5 element"); }
    if(!pool.offer(MESSAGE_6)) { Assert.fail("PoolCyclicExtensible: error in putting 6 element"); }
    if(!pool.offer(MESSAGE_7)) { Assert.fail("PoolCyclicExtensible: error in putting 7 element"); }

    m = pool.pool();
    Assert.assertSame("PoolCyclicExtensible: error in putting/retrieving 7 elements (1)", m, MESSAGE_1);
    m = pool.pool();
    Assert.assertSame("PoolCyclicExtensible: error in putting/retrieving 7 elements (2)", m, MESSAGE_2);
    m = pool.pool();
    Assert.assertSame("PoolCyclicExtensible: error in putting/retrieving 7 elements (3)", m, MESSAGE_3);
    m = pool.pool();
    Assert.assertSame("PoolCyclicExtensible: error in putting/retrieving 7 elements (4)", m, MESSAGE_4);
    m = pool.pool();
    Assert.assertSame("PoolCyclicExtensible: error in putting/retrieving 7 elements (5)", m, MESSAGE_5);
    m = pool.pool();
    Assert.assertSame("PoolCyclicExtensible: error in putting/retrieving 7 elements (6)", m, MESSAGE_6);
    m = pool.pool();
    Assert.assertSame("PoolCyclicExtensible: error in putting/retrieving 7 elements (7)", m, MESSAGE_7);

    if(!pool.isEmpty()) { Assert.fail("PoolCyclicExtensible should be empty"); }
  }
}

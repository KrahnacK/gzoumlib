/**************************************************************************/
/*  Implementation of a simple semi-unification algorithm (Henglein 1993) */
/*  This file is part of libgzoumix                                       */
/*  Copyright (C) 2014. Michael Lienhardt                                 */
/*                                                                        */
/*  This program is free software; you can redistribute it and/or modify  */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation; version 3 of the License.               */
/*                                                                        */
/*  This program is distributed in the hope that it will be useful, but   */
/*  WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     */
/*  General Public License for more details.                              */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with this program; if not, write to the Free Software           */
/*  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA         */
/*  02110-1301 USA                                                        */
/*                                                                        */
/**************************************************************************/

package org.gzoumix.logger;

import java.io.PrintStream;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Logger {

  public static enum Level { PANIC, ERROR, WARNING, DEBUG, NORMAL, INFO, ALL; };

  private static final String _C_tab = "  ";
  private static final int _C_subTab = _C_tab.length();

  private static class LoggedObject {
    private Level _level;
    private Object _object;
    private PrintStream _file;
    private String _indentLevel = "";

    public LoggedObject(Object object, PrintStream file, Level level) { _object = object; _file = file; _level = level; }
    public void setLevel(Level level) { this._level = level; }
    public Level getLevel() { return _level; }
    public String getIndentLevel() { return _indentLevel; }
    public PrintStream getFile() { return _file; }
    public void beginIndent() { _indentLevel = _indentLevel + _C_tab; }
    public void endIndent() { if (_indentLevel.length() > 0) _indentLevel = _indentLevel.substring(_C_subTab); }
  }

  private static Map<PrintStream, Set<LoggedObject>> _C_mapLogger;
  private static Map<Object, LoggedObject> _C_mapLogged;

  public static Object addLogFile(PrintStream file) {
    if(_C_mapLogger.get(file) != null) { return null; }
    else { _C_mapLogger.put(file, new HashSet<LoggedObject>()); return file; }
  }

  public static boolean addLoggedObject(Object object, PrintStream file, Level level) {
    Set<LoggedObject> set = _C_mapLogger.get(file);
    if(set == null) { return false; }
    else if(_C_mapLogged.get(object) != null) {return false; }
    else {
      LoggedObject tmp = new LoggedObject(object, file, level);
      set.add(tmp);
      _C_mapLogged.put(object, tmp);
      return true; }
  }

  public static boolean setLogLevel(Object object, Level level) {
    LoggedObject logObj = _C_mapLogged.get(object);
    if(logObj == null) { return false; }
    else { logObj.setLevel(level); return true; }
  }


  public void log(Object object, Level level, String message) {
    LoggedObject logObj = _C_mapLogged.get(object);
    if(logObj == null) { return; }
    else {
      if(level.ordinal() <= logObj.getLevel().ordinal()) {
        String string = logObj.getIndentLevel() + message.replace("\n", "\n" + logObj.getIndentLevel());
        logObj.getFile().println(string);
      }
    }
  }

  public void beginIndent(Object object) {
    LoggedObject logObj = _C_mapLogged.get(object);
    if(logObj == null) { return; }
    else { logObj.beginIndent(); }
  }

  public void endIndent(Object object) {    LoggedObject logObj = _C_mapLogged.get(object);
    if(logObj == null) { return; }
    else { logObj.endIndent(); }
  }


  // Util
  public void logPanic(Object object, String message)   { this.log(object, Level.PANIC, message); }
  public void logError(Object object, String message)   { this.log(object, Level.ERROR, message); }
  public void logWarning(Object object, String message) { this.log(object, Level.WARNING, message); }
  public void logDebug(Object object, String message)   { this.log(object, Level.DEBUG, message); }
  public void logNormal(Object object, String message)  { this.log(object, Level.NORMAL, message); }
  public void logInfo(Object object, String message)    { this.log(object, Level.INFO, message); }
  public void logAll(Object object, String message)     { this.log(object, Level.ALL, message); }

}

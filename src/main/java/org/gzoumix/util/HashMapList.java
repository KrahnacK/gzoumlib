/*
 * Copyright (C) 2015 Michael Lienhardt
 *
 * This file is part of Gzoumlib
 *
 * Gzoumlib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gzoumlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.gzoumix.util;

import java.util.*;


public class HashMapList<K,V> extends HashMap<K,List<V>> {

  public HashMapList() { super(); }
  public HashMapList(int initialCapacity) { super(initialCapacity); }
  public HashMapList(int initialCapacity, float loadFactor) { super(initialCapacity, loadFactor); }
  public HashMapList(Map<? extends K, ? extends List<V>> m) { super(m); }

  public List<V> putEl(K key, V value) {
    List<V> values = this.get(key);
    if(values == null) {
      values = new ArrayList<>();
      this.put(key, values);
    }
    values.add(value);
    return values;
  }

  @Override
  public void putAll(Map<? extends K,? extends List<V>> m) {
    for(Map.Entry<? extends K,? extends List<V>> entry: m.entrySet()) {
      List<V> list = this.get(entry.getKey());
      if(list != null) { list.addAll(entry.getValue()); }
      else { this.put(entry.getKey(), new ArrayList<V>(entry.getValue())); }
    }
  }
}

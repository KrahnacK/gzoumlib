/*
 * Copyright (C) 2015 Michael Lienhardt
 *
 * This file is part of Gzoumlib
 *
 * Gzoumlib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gzoumlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.gzoumix.util.agent;

import org.gzoumix.util.Log;
import org.gzoumix.util.Log.LogLevel;
import org.gzoumix.util.agent.runtime.RuntimeThread;
import org.gzoumix.util.agent.task.ITask;

import java.util.ArrayList;

/*
 * Structure of the runtime:
 *  - The runtime class set up the different system threads and necessary data, and starts everything
 *  - The runtime threads execute the user-based tasks.
 *     They also use signals for tasks synchronisation, and task load balancing.
 *     Moreover task have a synchronization ID
 */


public class Runtime {
  public Log logger;
  public Object runtimeLock;
  private int nbThreadStopped; //for graceful stop

  private ArrayList<RuntimeThread> threads;
  private int messageTTL;

  public Runtime() {
    this.runtimeLock = new Object();
    this.nbThreadStopped = 0;
    this.logger = new Log();
    this.threads = new ArrayList<>();
    this.messageTTL = -2;
  }
  public void log(LogLevel level, Object message) { this.logger.log(level, message); /*System.out.println(message);*/ }

  public void add(RuntimeThread thread) {
    this.messageTTL++;

    if(this.threads.isEmpty()) {
      this.threads.add(thread);
      thread.init(thread);
    } else {
      RuntimeThread prev = this.threads.get(0);
      this.threads.add(thread);
      thread.init(prev.getNext());
      prev.init(thread);
    }
  }

  public void start() {
    for(RuntimeThread thread: this.threads) {
      thread.start();
    }
  }

  public void stop() {
    int nbThreads = getNBThread();
    for(RuntimeThread thread: this.threads) {
      thread.stop();
    }
    //wait for all threads to finish their tasks
    synchronized (this.runtimeLock) {
      while(nbThreads > this.nbThreadStopped) {
        log(Log.LogLevel.DEBUG, "[runtime stop] 1 thread stopped, nbThreads:"+nbThreads+"  nbThreadStopped:"+this.nbThreadStopped);
        try { this.runtimeLock.wait(); }
        catch (InterruptedException e) { e.printStackTrace(); }
      }
    }
    log(Log.LogLevel.DEBUG, "[runtime stop] all threads stopped");
  }
  
  public void threadStop() { //internal, called by RuntimeThread
    synchronized(this.runtimeLock) {
      //TODO: properly identify the thread to remove it from this.threads and the threads linked list
      nbThreadStopped++;
      this.runtimeLock.notify();
    }
  }

  public int getNBThread() { return this.threads.size(); }
  public int getMessageTTL() { return messageTTL; }

  public RuntimeThread threadInChargeOf(Object group) {
    //the constrain here is that 2 tasks from the same group MUST ALWAYS be executed by the same thread
    //there is no constrain whatsoever on tasks from different groups (user has to be careful when defining groups)
    //using hashCode on the group may be too conservative, but it is always correct 
    int index = group.hashCode() % this.threads.size();
    return this.threads.get(index);
  }

  ///////////////////////////////////////////////////////////////////////////////
  // proxy to RuntimeThread methods

  public void task(ITask task) {
    RuntimeThread t = threadInChargeOf(task.getGroup());
    if (t != null) {
      log(Log.LogLevel.DEBUG, "task:"+task+" assigned to thread:"+t.id()+" group:"+task.getGroup()+"\n");
      t.task(task);
    } else
      log(Log.LogLevel.ERROR, "Cannot assign a thread to task:"+task+" task's group:"+task.getGroup()+"\n");
  }
  public void signal(Object signal) {
    if(!this.threads.isEmpty()) {
      this.threads.get(0).signal(signal);
    }else
      log(Log.LogLevel.ERROR, "No thread to signal!\n");
  }

}

/*
 * Copyright (C) 2015 Michael Lienhardt
 *
 * This file is part of Gzoumlib
 *
 * Gzoumlib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gzoumlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.gzoumix.util.agent.data;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

// this is just a toy implementation, to show that cyclic pools can be extended
// So this structure can be used in practice
// Of course in practice, one would put the list structure directly in the PoolCyclic struct.
public class PoolCyclicExtensible<E> {


  private int capacity;
  private List<PoolCyclic<E>> content;
  private List<PoolCyclic<E>> ppool;

  public PoolCyclicExtensible(int capacity) {
    this.capacity = capacity;

    this.content = new ArrayList<>();
    this.ppool = new ArrayList<>();
  }

  public int size() {
    int res = 0;
    for (PoolCyclic<E> pool : content) {
      res = res + pool.size();
    }
    return res;
  }

  public boolean isEmpty() {
    return this.content.isEmpty();
  }
  // no notion of full, as it is extensible

  public boolean add(E e) {
    if (!this.offer(e)) {
      throw new IllegalStateException();
    } else {
      return true;
    }
  }

  public boolean offer(E e) {
    if (this.isEmpty() || this.content.get(0).isFull()) { // need to add a pool
      if (this.ppool.isEmpty()) {
        this.content.add(0, new PoolCyclic<E>(this.capacity));
      } else {
        this.content.add(0, this.ppool.remove(0));
      }
    }
    this.content.get(0).offer(e);
    return true;
  }

  public E element() {
    if(this.isEmpty()) { throw new NoSuchElementException(); }
    else { return this.peek(); }
  }

  public E peek() { return this.isEmpty() ? null : this.content.get(this.content.size() -1).peek(); }


  public E pool() {
    if(this.isEmpty()) { return null; }
    PoolCyclic<E> pool = this.content.get(this.content.size() - 1);
    E res = pool.pool();
    if(pool.isEmpty()) {
      this.content.remove(this.content.size() - 1);
      this.ppool.add(0, pool);
    }
    return res;
  }

  public E remove() {
    if(this.isEmpty()) { return null; }
    PoolCyclic<E> pool = this.content.get(this.content.size() - 1);
    E res = pool.remove();
    if(pool.isEmpty()) {
      this.content.remove(this.content.size() - 1);
      this.ppool.add(0, pool);
    }
    return res;
  }


}

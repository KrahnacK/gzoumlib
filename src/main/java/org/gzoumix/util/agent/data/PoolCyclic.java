/*
 * Copyright (C) 2015 Michael Lienhardt
 *
 * This file is part of Gzoumlib
 *
 * Gzoumlib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gzoumlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.gzoumix.util.agent.data;

import java.util.NoSuchElementException;

public class PoolCyclic<E> {

  private Object array[];
  private int begin;
  private int end;

  public PoolCyclic(int capacity) {
    this.array = new Object[capacity]; //(E[]) Array.newInstance(c, capacity);
    this.begin = 0;
    this.end = 0;
  }

  public int capacity() { return this.array.length; }

  public int size() {
    int val = this.end - this.begin;
    if(val == 0) {
       return (this.array[this.begin] == null) ? 0 : this.array.length;
    } else { return val > 0 ? val : this.capacity() - this.begin + this.end; }
  }

  public boolean isEmpty() { return (this.begin == this.end) && (this.array[this.begin] == null); }
  public boolean isFull() { return (this.begin == this.end) && (this.array[this.begin] != null); }

  // Shallow implementation of the Queue interface (but not the Collection one)

  public boolean add(E e) {
    if(!this.offer(e)) { throw new IllegalStateException(); }
    else { return true; }
  }

  public boolean offer(E e) {
    if(this.isFull()) { return false; }
    this.array[this.end++] = e;
    if(this.end == this.array.length) { this.end = 0; }
    return true;
  }


  public E element() {
    if(this.isEmpty()) { throw new NoSuchElementException(); }
    else { return this.peek(); }
  }

  public E peek() { return this.isEmpty() ? null : (E)this.array[this.begin]; }

  public E pool() {
    if(this.isEmpty()) { return null; }
    E res = this.peek();
    this.array[this.begin] = null;
    if(++this.begin == this.array.length) { this.begin = 0; }
    return res;
  }

  public E remove() {
    E res = this.element();
    if(res != null) {
      this.array[this.begin] = null;
      if(++this.begin == this.array.length) { this.begin = 0; }
    }
    return res;
  }

}


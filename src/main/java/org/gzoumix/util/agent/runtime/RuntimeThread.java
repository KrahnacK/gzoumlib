/*
 * Copyright (C) 2015 Michael Lienhardt
 *
 * This file is part of Gzoumlib
 *
 * Gzoumlib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gzoumlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.gzoumix.util.agent.runtime;

import org.gzoumix.util.HashMapSet;
import org.gzoumix.util.Log;
import org.gzoumix.util.agent.Runtime;
import org.gzoumix.util.agent.data.PoolCyclic;
import org.gzoumix.util.agent.task.ITask;

import java.util.*;

public class RuntimeThread implements Runnable {



  private Runtime runtime;        // the runtime of the current thread (needed to know the messages' TTL)
  private RuntimeThread next;     // the next thread in the thread message ring
  private Thread thread;          // the Java thread of the current thread
  private Object lock;            // used to make that thread sleep;
  private boolean keepup;         // global switch to stop the thread
  private int id;

  private PoolCyclic<RuntimeMessage> inbox; // the inbox of the thread
  private final DeliverMail mailman = new DeliverMail(this); // the task managing the inbox
  // (everything should be handled by user-level tasks, even Garbage Collection)


  private int nbtask;                        // nb of tasks this thread have to execute
  private HashMapSet<Object, ITask> waiting; // tasks waiting for some signals, with the signals being the keys
  private Queue<ITask> active;               // tasks ready to execute
  private HashMapSet<Object, ITask> groups;  // the groups managed by the current thread


  public RuntimeThread(Runtime runtime, int inCapacity, int id) {
    this.runtime = runtime;
    this.next = null;
    this.lock = new Object();
    this.keepup = true;
    this.id = id;

    this.inbox = new PoolCyclic<>(inCapacity);

    this.nbtask = 0;
    this.waiting = new HashMapSet<>(); // this should be implemented in C with a hashmap with internal memory management
    this.active = new LinkedList<>();  // this should be implemented in C with the same data-structure as in Linux
    // in practice, a priority system could also be useful. We'll see later

    this.groups = new HashMapSet<>();
  }


  ///////////////////////////////////////////////////////////////////////////////
  // setup and loop methods

  public void init(RuntimeThread next) { this.next = next; }
  public RuntimeThread getNext() { return next; }
  public int id() { return this.id; }
  public void log(Log.LogLevel level, Object message) {
    runtime.log(level, "[Thread "+this.id+"]"+message+"\n");
  }

  public void start() {
    this.thread = new Thread(this);
    this.thread.start();
  }

  @Override
  public void run() {
    while(keepup || this.nbtask + this.active.size() > 0) { //conservative estimate to avoid stopping too soon
      while (!this.active.isEmpty()) {
        // 1. execute the task
        ITask task = null;
        synchronized (this.lock) {
          task = this.active.poll();
        }
        this.log(Log.LogLevel.DEBUG, "executing task:"+task+"\n");
        Collection<ITask> nexts = task.run();
        if (task != mailman) {
          this.nbtask--;
        }

        // 2. update the group set
        Set<ITask> group = this.groups.get(task.getGroup());
        if(group != null) { // check, for the group-less ad-hoc tasks (message queue manager)
          group.remove(task);
          if (group.isEmpty()) { this.groups.remove(task.getGroup()); }
        }

        // 3. manage the continuations
        if(nexts != null) { for (ITask ntask : nexts) { this.runtime.task(ntask); }}
      }

      if (keepup) {
        // nothing to execute anymore: go to sleep
        this.log(Log.LogLevel.DEBUG, "goes to sleep\n");
        synchronized (this.lock) {
          while(keepup && this.active.isEmpty()) {
            try { this.lock.wait(); }
            catch (InterruptedException e) { e.printStackTrace(); }
          }
        }
        this.log(Log.LogLevel.DEBUG, "wakes up, keepup:"+keepup);
      }
    }
    this.log(Log.LogLevel.DEBUG, "stopping thread:"+this.id);
    this.runtime.threadStop();
  }

  public void stop() {
    this.keepup = false;
    synchronized (this.lock) {
    if (this.active.isEmpty()) {
        this.lock.notify();
      }}
  }

  public void signal(Object signal) {
    this.signalLocal(signal);
    this.next.receive(RuntimeMessage.SIGNAL(this.runtime.getMessageTTL(), signal));
  }

  public void task(ITask task) {
    //the call comes from Runtime and we are sure that task must be executed by the local thread
    
    if (Thread.currentThread() == this.thread) {
      //optim: call synchronously and avoid taking the lock from receive when not needed
      //happens when handling continuations
      taskLocal(task);
    } else {
      this.receive(RuntimeMessage.TASK_INSTALL(this.runtime.getMessageTTL(), task, this));
    }
  }


  ///////////////////////////////////////////////////////////////////////////////
  // management methods

  private void receive(RuntimeMessage message) {
    this.log(Log.LogLevel.DEBUG, " message:"+message+" is being sent to thread"+this.id);
    synchronized (this.lock) {
      boolean wakeup = this.active.isEmpty();
      this.inbox.add(message);
      this.active.add(this.mailman);
      if (wakeup)
        this.lock.notify();
    }
    
    /*
    boolean restart = this.active.isEmpty(); //possibly a racy access

    this.inbox.add(message); //not thread-safe
    this.active.add(this.mailman); //not thread-safe but we don't care
    if (restart) {
      synchronized (this.lock) { // you've got mail!
        this.active.add(this.mailman);
        this.lock.notify(); }
    }
     */
  }


  private void signalLocal(Object signal) {
    Set<ITask> tasks = this.waiting.get(signal);
    this.log(Log.LogLevel.DEBUG, "signalling local signal:"+signal+" (tasks:"+tasks+")\n");

    if(tasks == null) { return; }

    Iterator<ITask> itask = tasks.iterator();

    while(itask.hasNext()) {
      ITask task = itask.next();
      Object[] guard = task.getGuard();
      boolean move = true;
      for(int i = 0; i < guard.length; i++) {
        if(guard[i] != null) {
          if(guard[i] == signal) { guard[i] = null; }
          else { move = false; }
        }
      }
      // move the task if necessary
      if(move) {
        itask.remove();
        synchronized (this.lock) {
          this.active.add(task);
        }
        this.log(Log.LogLevel.DEBUG, "signalling local signal:"+signal
            +" (tasks:"+tasks+") nbtask:"+this.nbtask+" activeTasks:"+this.active+"\n");
      }
    }

    // Garbage Collection
    if(tasks.isEmpty()) { this.waiting.remove(signal); }
  }

  private void taskLocal(ITask task) {
    this.log(Log.LogLevel.DEBUG, "enqueuing task:"+task+" (group:"+task.getGroup()+")\n");

    this.nbtask++;
    this.groups.putEl(task.getGroup(), task); // add to the relevant group

    boolean active = true;
    Object[] guard = task.getGuard();
    if(guard != null) { for(int i = 0; i < guard.length; i++) {
      if(guard[i] != null) {
        active = false;
        this.waiting.putEl(guard[i], task);
      }
    }}

    if(active) { 
      synchronized (this.lock) {this.active.add(task); }}
  }


  ///////////////////////////////////////////////////////////////////////////////
  // management class

  public static class DeliverMail implements ITask {

    // no next task scheduled: triggered only when receiving messages
    private RuntimeThread thread;

    public DeliverMail(RuntimeThread thread) { this.thread = thread; }

    @Override
    public Collection<ITask> run() {
      // Loop on the mail, and deliver it to the right mailbox, or activate the right predicate
      while(!this.thread.inbox.isEmpty()) {
        RuntimeMessage message = this.thread.inbox.pool();
        thread.log(Log.LogLevel.DEBUG, "mailman receives message:"+message+"\n");

        // 1. manage the message
        if(message.getKind() == RuntimeMessage.Kind.SIGNAL) {
          this.thread.signalLocal(message.getSignal());
        } else if(message.getKind() == RuntimeMessage.Kind.TASK_INSTALL) {
          if(message.getThread() == this.thread) {
            this.thread.taskLocal(message.getTask());
            return null; // skip the message forward
          }
        }
        // 2. forward it if necessary
        if(message.getTTL() > 0) {
          message.step();
          this.thread.next.receive(message);
        }
      }
      return null;
    }

    @Override
    public Object[] getGuard() { return null; } // is always "active"

    @Override
    public Object getGroup() { return this.thread; } // is local to this thread.
  }



}

/*
 * Copyright (C) 2015 Michael Lienhardt
 *
 * This file is part of Gzoumlib
 *
 * Gzoumlib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gzoumlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.gzoumix.util.agent.runtime;

import org.gzoumix.util.agent.task.ITask;

public class RuntimeMessage {
  public static class Kind {
    public static final Kind SIGNAL = new Kind();
    public static final Kind TASK_INIT = new Kind();
    public static final Kind TASK_INSTALL = new Kind();
  }

  private Kind kind;


  public static RuntimeMessage SIGNAL(int ttl, Object signal) {
    return new RuntimeMessage(Kind.SIGNAL, ttl, signal, null, null, 0);
  }

  public static RuntimeMessage TASK_INIT(int ttl, ITask task, RuntimeThread thread, int min) {
    return new RuntimeMessage(Kind.TASK_INIT, ttl, 0, task, thread, min);
  }

  public static RuntimeMessage TASK_INSTALL(int ttl, ITask task, RuntimeThread thread) {
    return new RuntimeMessage(Kind.TASK_INSTALL, ttl, 0, task, thread, 0);
  }


  private int ttl;


  private Object signal; // SIGNAL

  private ITask task; // TASK
  private RuntimeThread thread;
  private int min;


  private RuntimeMessage(Kind kind, int ttl, Object signal, ITask task, RuntimeThread thread, int min) {
    this.kind = kind;
    this.ttl = ttl;
    this.signal = signal;
    this.task = task;
    this.thread = thread;
    this.min = min;
  }

  @Override
  public String toString() {
    String s = new String(" ttl:"+ttl+" min:"+min+" task:"+task);
    if (kind == Kind.SIGNAL)
      s = s+" SIGNAL";
    else if (kind == Kind.TASK_INIT)
      s = s+" TASK_INIT";
    else if (kind == Kind.TASK_INSTALL)
      s = s+" TASK_INSTALL";

    if (this.thread != null)
      s = s + " thread:"+thread.id();
    else
      s = s + " thread:null";
    return s;
  }

  public Kind getKind() { return kind; }
  public int getTTL() { return ttl; }
  public Object getSignal() { return signal; }
  public ITask getTask() { return task; }
  public RuntimeThread getThread() { return thread; }
  public int getMin() { return min; }

  public void step() { this.ttl--; }
  public void updateThread(RuntimeThread thread, int min) { this.thread = thread; this.min = min; }

}

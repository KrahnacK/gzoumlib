/*
 * Copyright (C) 2015 Michael Lienhardt
 *
 * This file is part of Gzoumlib
 *
 * Gzoumlib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gzoumlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.gzoumix.util;

import org.gzoumix.util.formula.FormulaFalse;
import org.gzoumix.util.formula.FormulaTrue;

public class Global {
  public static final Log log = new Log();

  public static class Formula {
    public static final String TRUE = "true";
    public static final String FALSE = "false";

    public static final String NOT  = "!";
    public static final String AND = " & ";
    public static final String OR = " | ";
    public static final String IMPLIES = " => ";
    public static final String EQUIV = " <=> ";

    public static final FormulaTrue FORMULA_TRUE = new FormulaTrue(null);
    public static final FormulaFalse FORMULA_FALSE = new FormulaFalse(null);
  }
}

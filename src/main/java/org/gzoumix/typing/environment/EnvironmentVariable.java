/**************************************************************************/
/*  Implementation of a simple semi-unification algorithm (Henglein 1993) */
/*  This file is part of libgzoumix                                       */
/*  Copyright (C) 2014. Michael Lienhardt                                 */
/*                                                                        */
/*  This program is free software; you can redistribute it and/or modify  */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation; version 3 of the License.               */
/*                                                                        */
/*  This program is distributed in the hope that it will be useful, but   */
/*  WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     */
/*  General Public License for more details.                              */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with this program; if not, write to the Free Software           */
/*  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA         */
/*  02110-1301 USA                                                        */
/*                                                                        */
/**************************************************************************/

package org.gzoumix.typing.environment;


import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class EnvironmentVariable<N,V> {

  private List<Map<N, V>> _variables;  // stack frame of mapping variable => record or future

  public EnvironmentVariable() { _variables = new LinkedList<Map<N, V>>(); }

  public V getVariable(N name) {
    V res = null;
    for(Map<N, V> map: _variables) {
      res = map.get(name);
      if(res != null) return res;
    }
    return res;
  }

  public V getField(N name) { // helper method for object-oriented programming
    Map<N, V> tmp = _variables.remove(0);
    V res = this.getVariable(name);
    _variables.add(0, tmp);
    return res;
  }


  public void newScope() { _variables.add(0, new HashMap<N, V>()); }
  public void popScope() { _variables.remove(0); }
  public void putVariable(N name, V v) { _variables.get(0).put(name, v); }

  public void clear() { _variables = new LinkedList<Map<N, V>>(); }
  public void init()  { this.clear(); this.newScope(); }

}

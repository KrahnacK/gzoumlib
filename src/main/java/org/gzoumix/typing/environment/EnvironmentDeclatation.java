/**************************************************************************/
/*  Implementation of a simple semi-unification algorithm (Henglein 1993) */
/*  This file is part of libgzoumix                                       */
/*  Copyright (C) 2014. Michael Lienhardt                                 */
/*                                                                        */
/*  This program is free software; you can redistribute it and/or modify  */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation; version 3 of the License.               */
/*                                                                        */
/*  This program is distributed in the hope that it will be useful, but   */
/*  WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     */
/*  General Public License for more details.                              */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with this program; if not, write to the Free Software           */
/*  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA         */
/*  02110-1301 USA                                                        */
/*                                                                        */
/**************************************************************************/

package org.gzoumix.typing.environment;


import java.util.*;

public class EnvironmentDeclatation<N,D extends IDeclaration<N, D>> {
  // An environment maps  path to a definition which basically follows the module structure
  // A path is a sequence of elements of type N
  // A definition is of type D

  // Basically the usage of this environment is as follows:
  // classically, in a module, you can declare some imports: this corresponds to calling the right "addSearchPath" method
  // then, when an element is declared, you call the "put" method. That element can have dependencies to other data.
  // For instance, one can declare
  // type int_list = int list
  // and so that type depends on "int" and "list"
  // the "put" method allows you to declare such dependencies, and when the declaration of these elements will be added to the environment,
  // that way we don't have to order all
  // these dependencies will be resolved by the environment by calling the method "resolve" of the depending declaration.
  // Finally, the "get" method returns the declaration corresponding to the specified path (using the currently active seachPaths), or null if there are no such declaration.

  // This implementation of an environment should be expressive enough for all my usages.

  private static final String _C_tab = "  ";

  /*******************************/
  /** 1. Local data structures   */
  /*******************************/
    // Paths for declaration and import
  public static class Path<N> extends LinkedList<N> {
    public Path() { super(); }
    public Path(Collection<? extends N> c) { super(c); }
  }
  public static class PathStar<N> extends Path<N> {
    public PathStar() { super(); }
    public PathStar(Collection<? extends N> c) { super(c); }
  }
  public static class PathElement<N> extends Path<N> {
    public PathElement() { super(); }
    public PathElement(Collection<? extends N> c) { super(c); }
    public PathElement(N el) { super(); this.add(el); }

    public boolean isIn(PathStar<N> path) {
      if(this.size() -1 == path.size()) {
        Iterator<N> ip = path.iterator();
        Iterator<N> ie = this.iterator();
        while(ip.hasNext()) { if(!ip.next().equals(ie.next())) { return false; } }
        return true;
      } else { return false; }
    }

    public PathElement<N> match(SearchPath<N> sp) {
      PathElement<N> res = null;
      if(sp.getPath() instanceof PathElement) { // 3 cases: not open, open, or name
        if(this.equals(sp.getPath())) { res = (PathElement<N>)sp.getPath(); }
        if((res == null) && (sp.isOpen()) && (this.size() == 1) && (this.getLast().equals(sp.getPath().getLast()))) { res = (PathElement<N>)sp.getPath(); }
        if((res == null) && (this.size() == 1) && (this.getLast().equals(sp.getName()))) { res = (PathElement<N>)sp.getPath(); }
      } else { // 3 cases: not open, open, or name
        PathElement<N> tmp = new PathElement<N>(); tmp.addAll(sp.getPath()); tmp.add(this.getLast());
        if((this.isIn((PathStar<N>) sp.getPath()))) {  res = tmp; }
        if((res == null) && (this.size() == 2) && (this.getFirst().equals(sp.getName()))) { res = tmp; }
        if((res == null) && (sp.isOpen())) { tmp = new PathElement<N>(); tmp.addAll(sp.getPath()); tmp.addAll(this); res = tmp; }
      }
      return res;
    }
  }
  public static class SearchPath<N> {
    private Path<N> _path;
    private N _name;       // Alternative name for that path
    private boolean _open; // if the path is opened or not (like in Ada, a path can be imported but not opened)

    public SearchPath(Path<N> path, N name, boolean open) {
      _path = path; _name = name; _open = open;
    }
    public SearchPath(Path<N> path, boolean open) { this(path, null, open); }

    public Path<N> getPath() { return _path; }
    public N getName() { return _name; }
    public boolean isOpen() { return _open; }
    public int hashCode() { return _path.hashCode(); }
    public String toString() { return "(" + getPath().toString() + ", " + (getName() == null ? "" : getName().toString() + ", ") + String.valueOf(isOpen()) + ")"; }
  }

  public static final SearchPath emptyOpenPath = new SearchPath(new PathStar(), null, true);

    // Dependencies
  public static class Dependency<N,D extends IDeclaration<N, D>> {
    private D _request;
    private PathElement<N> _dependency;
    private List<SearchPath<N>> _searchPaths;

    public Dependency(D req, PathElement<N> el, List<SearchPath<N>> paths) {
      _request = req;
      _dependency = el;
      _searchPaths = new LinkedList<SearchPath<N>>(paths);
    }

    public D getRequester() { return _request; }
    public PathElement getDependency() { return _dependency; }
    public List<SearchPath<N>> getSeachPaths() { return _searchPaths; }

    public String toString() {
      String res =  "req = \"" + getRequester().getName() + "\", dep = \"" + getDependency().toString() + "\", paths = " + getSeachPaths().toString();
      return res;
    }
  }

  /*******************************/
  /** 2. Fields and constructor  */
  /*******************************/
  private List<SearchPath<N>> _searchPaths;
  private Map<PathElement<N>, Set<Dependency<N,D>>> _dependencies;
  private Map<PathElement<N>,D> _definitions;

  public EnvironmentDeclatation() {
    _searchPaths = new LinkedList<SearchPath<N>>();
    _dependencies = new HashMap<PathElement<N>, Set<Dependency<N, D>>>();
    _definitions = new HashMap<PathElement<N>, D>();
  }

  /*******************************/
  /** 3. visibility manipulation */
  /*******************************/
  public void addSearchPath(SearchPath<N> path) { _searchPaths.add(0, path); }
  public void addSearchPath(Path<N> path, N name, boolean open) { addSearchPath(new SearchPath<N>(path, name, open)); }
  public void resetSearchPath() { _searchPaths = new LinkedList<SearchPath<N>>(); }


  /*******************************/
  /** 4. Get                     */
  /*******************************/
  public D get(PathElement<N> e) {
    D res = null;
    for(SearchPath<N> sp: _searchPaths) {
    /*
      if(sp.getPath() instanceof PathElement) { // 3 cases: not open, open, or name
        if(e.equals(sp.getPath())) { res = _definitions.get((PathElement<N>)sp.getPath()); }
        if((res == null) && (sp.isOpen()) && (e.size() == 1) && (e.getLast().equals(sp.getPath().getLast()))) { res = _definitions.get((PathElement<N>)sp.getPath()); }
        if((res == null) && (e.size() == 1) && (e.getLast().equals(sp.getName()))) { res = _definitions.get((PathElement<N>)sp.getPath()); }
      } else { // 3 cases: not open, open, or name
        PathElement<N> tmp = new PathElement<N>(); tmp.addAll(sp.getPath()); tmp.add(e.getLast());
        if((e.isIn((PathStar<N>)sp.getPath())) || (sp.isOpen())) { res = _definitions.get(tmp); }
        if((res == null) && (e.size() == 2) && (e.getFirst().equals(sp.getName()))) { res = _definitions.get(tmp); }
      }*/
      res = _definitions.get(e.match(sp));
      if (res != null) { return res; }
    }
    return null;
  }


  /*******************************/
  /** 5. put                     */
  /*******************************/
  public boolean put(PathElement<N> path,D def, Set<PathElement<N>> depends) {
    boolean res = true;
    _definitions.put(path, def);

    // 1, solve the registered dependencies
    Set<Dependency<N, D>> tosolve = _dependencies.get(path);
    if (tosolve != null) {
      Set<Dependency<N, D>> solved = new HashSet<Dependency<N, D>>();
      List<SearchPath<N>> currentSearchPath = _searchPaths;
      for (Dependency<N, D> dep : tosolve) {
        _searchPaths = dep.getSeachPaths();
        if(get(dep.getDependency()) != null) {
          dep.getRequester().resolve(dep.getDependency(), def);
          solved.add(dep);
        }
      }
      for (Dependency<N, D> dep : solved) { // remove all reference to that dependency
        for(Set<Dependency<N,D>> set : _dependencies.values()) {
          if(set != null) { set.remove(dep); }
        }
      }
      _searchPaths = currentSearchPath;
    }

    // 2. solve depends
    if(depends != null) for(PathElement<N> el: depends) {
      D def2 = this.get(el);
      if (def2 != null) {
        def.resolve(el, def2);
      } else { // add the dependency to all path that can solve it
        res = false;
        LinkedList<SearchPath<N>> depsearch = new LinkedList<SearchPath<N>>(_searchPaths);
        Dependency<N, D> dep = new Dependency<N, D>(def, el, depsearch);
        for (SearchPath<N> sp : depsearch) {
          PathElement<N> petmp = el.match(sp);
          if(petmp != null) {
            Set<Dependency<N,D>> setdeptmp = _dependencies.get(petmp);
            if(setdeptmp != null) { setdeptmp.add(dep); }
            else { setdeptmp = new HashSet<Dependency<N, D>>(); setdeptmp.add(dep); _dependencies.put(petmp, setdeptmp); }
          }
        }
      }
    }
    return res;
  }


  /*******************************/
  /** 6. Utility Functions       */
  /*******************************/

  public boolean hasDependency() {
    for(Set<Dependency<N,D>> deps: _dependencies.values()) { if(deps.size() > 0) return true; }
    return false;
  }

  public EnvironmentDeclatation<N,D> copy() {
    EnvironmentDeclatation<N,D> res = new EnvironmentDeclatation<>();
    res._searchPaths = new LinkedList<>(this._searchPaths);
    res._dependencies = new HashMap<>(this._dependencies);
    res._definitions = new HashMap<>(this._definitions);
    return res;
  }

  public String toString() {
    Map<D, Set<Dependency<N,D>>> mapDef = new HashMap<>();
    Map<Dependency<N,D>, Integer> mapDep = new HashMap<>();
    int idDep = 0;
    for(Set<Dependency<N,D>> set : _dependencies.values()) {
      for(Dependency<N,D> dep : set) {
        if(mapDep.get(dep) == null) {
          Set<Dependency<N,D>> stmp = mapDef.get(dep.getRequester());
          if(stmp == null) { stmp = new HashSet<>(); mapDef.put(dep.getRequester(), stmp); }
          stmp.add(dep);
          mapDep.put(dep, new Integer(idDep++));
        }
      }
    }
    Map<D, Set<Integer>> mapDefIdDep = new HashMap<>();
    for(Map.Entry<D, Set<Dependency<N,D>>> entry : mapDef.entrySet()) {
      Set<Integer> settmp = new HashSet<>(); mapDefIdDep.put(entry.getKey(), settmp);
      for(Dependency<N,D> dep : entry.getValue()) { settmp.add(mapDep.get(dep)); }
    }

    String res = "Declarations:\n";
    for(Map.Entry<PathElement<N>,D> entry: _definitions.entrySet()) {
      res = res + _C_tab + entry.getKey().toString() + "\n" + _C_tab + _C_tab + entry.getValue().toString().replace("\n", "\n" + _C_tab + _C_tab);
      res = res +  _C_tab + _C_tab + "dependencies = " + mapDefIdDep.get(entry.getValue()) + "\n";
    }
    res = res + "Dependencies:" ;
    for(Map.Entry<Dependency<N,D>, Integer> entry : mapDep.entrySet()) {
      res = res + "\n" + _C_tab + _C_tab + entry.getValue() + " : " + entry.getKey().toString().replace("\n", "\n" + _C_tab + _C_tab);
    }
    res = res + "\nPaths: " + _searchPaths.toString();
    return res;
  }

}

/**************************************************************************/
/*  Implementation of a simple semi-unification algorithm (Henglein 1993) */
/*  Copyright (C) 2012. Michael Lienhardt                                 */
/*                                                                        */
/*  This program is free software; you can redistribute it and/or modify  */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation; version 2 of the License.               */
/*                                                                        */
/*  This program is distributed in the hope that it will be useful, but   */
/*  WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     */
/*  General Public License for more details.                              */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with this program; if not, write to the Free Software           */
/*  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA         */
/*  02110-1301 USA                                                        */
/*                                                                        */
/**************************************************************************/

package org.gzoumix.typing.solver.semisolver.todelete;

import java.util.List;

import org.gzoumix.typing.solver.semisolver.edge.EdgeSemi;
import org.gzoumix.typing.solver.semisolver.edge.EdgeUnif;
import org.gzoumix.typing.solver.semisolver.edge.IEdge;
import org.gzoumix.typing.solver.term.TermVariable;

public abstract class History {

  private IEdge edge;

  /* 1. Basic Set/Get */
  void setEdge(IEdge e) { edge = e; }
  public IEdge getEdge() { return edge; }

  /* 2. Basic Test */
  public boolean isVariableHistory() { return (edge.getLeft() instanceof TermVariable) && (edge.getRight() instanceof TermVariable); }
  public boolean isSemiEdge() { return (edge instanceof EdgeSemi); }
  public boolean isUnifEdge() { return (edge instanceof EdgeUnif); }
  public boolean hasStructured() { return !this.isVariableHistory(); }


  /* 3. Basic Retrival */
  public TermVariable getFollowing(TermVariable v) {
    if((this.isVariableHistory()) && (this.isUnifEdge())) {
      TermVariable vl = ((TermVariable)edge.getLeft());
      TermVariable vr = ((TermVariable)edge.getRight());
      if(vl == v) { return vr; }
      if(vr == v) { return vl; }
    }
    return null;
  }

  public TermVariable getVariable() {
    if(edge.getLeft() instanceof TermVariable) { return (TermVariable)edge.getLeft(); }
    if(edge.getRight() instanceof TermVariable) { return (TermVariable)edge.getRight(); }
    return null;
  }

  public Integer getI() { return ((EdgeSemi)edge).getI(); }

  public abstract List<Information> getInformations();

}



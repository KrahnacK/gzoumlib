/**************************************************************************/
/*  Implementation of a simple semi-unification algorithm (Henglein 1993) */
/*  This file is part of libgzoumix                                       */
/*  Copyright (C) 2014. Michael Lienhardt                                 */
/*                                                                        */
/*  This program is free software; you can redistribute it and/or modify  */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation; version 3 of the License.               */
/*                                                                        */
/*  This program is distributed in the hope that it will be useful, but   */
/*  WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     */
/*  General Public License for more details.                              */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with this program; if not, write to the Free Software           */
/*  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA         */
/*  02110-1301 USA                                                        */
/*                                                                        */
/**************************************************************************/

package org.gzoumix.typing.solver.semisolver.error;

import org.gzoumix.typing.solver.semisolver.edge.EdgeSemi;
import org.gzoumix.typing.solver.semisolver.edge.EdgeUnif;
import org.gzoumix.typing.solver.semisolver.edge.IEdge;
import org.gzoumix.typing.solver.semisolver.history.EnumHistoryStep;
import org.gzoumix.typing.solver.semisolver.history.IHistory;
import org.gzoumix.typing.solver.term.ITerm;

import java.util.LinkedList;
import java.util.List;

public class ErrorUnif<I extends IHistory> implements IError {

  private IEdge edge;

  /* Constructors */

  public ErrorUnif(IHistory<I> history, ITerm t1, ITerm t2) {
    List<IHistory<I>> tmp = new LinkedList<IHistory<I>>(); tmp.add(history);
    edge = new EdgeUnif(tmp, EnumHistoryStep.BASE, t1, t2);
  }

  public ErrorUnif(IHistory<I> history, ITerm t1, Integer i, ITerm t2) {
    List<IHistory<I>> tmp = new LinkedList<IHistory<I>>(); tmp.add(history);
    edge = new EdgeSemi(tmp, EnumHistoryStep.BASE, t1, i, t2);
  }

  /* Basic Access */
  public IEdge getEdge() { return edge; }

}



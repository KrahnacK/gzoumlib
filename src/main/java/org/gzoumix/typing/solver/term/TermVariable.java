/**************************************************************************/
/*  Implementation of a simple semi-unification algorithm (Henglein 1993) */
/*  This file is part of libgzoumix                                       */
/*  Copyright (C) 2014. Michael Lienhardt                                 */
/*                                                                        */
/*  This program is free software; you can redistribute it and/or modify  */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation; version 3 of the License.               */
/*                                                                        */
/*  This program is distributed in the hope that it will be useful, but   */
/*  WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     */
/*  General Public License for more details.                              */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with this program; if not, write to the Free Software           */
/*  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA         */
/*  02110-1301 USA                                                        */
/*                                                                        */
/**************************************************************************/

package org.gzoumix.typing.solver.term;

import java.util.HashSet;
import java.util.Set;

public class TermVariable implements ITerm {

  public static int _C_varCounter = 0;
  private int _id;

  TermVariable() {
    _id = _C_varCounter;
    _C_varCounter += 1;
  }


  /* 1. Term */

  public boolean strictlyContains(ITerm t) { return false; }
  public boolean hasNull() { return false; }


  public Set<TermVariable> fv() {
    Set<TermVariable> res = new HashSet<TermVariable>();
    res.add(this);
    return res;
  }

  public ITerm subst(TermVariable v, ITerm t) {
    if(this.equals(v)) { return t; }
    else {return this; }
  }

  /* 2. Comparable */

  @Override
  public int compareTo(Object t) {
    if(this == t) { return 0; }
    if(t instanceof TermVariable) { return this._id - ((TermVariable)t)._id; }
    else { return -1; }
  }

  /* 3. Object */

  @Override
  public boolean equals(Object t) {
    if (t == this) { return true; }
    else if ((t != null) && (t instanceof TermVariable)){ return _id == ((TermVariable)t)._id; }
    else { return false; }
  }

  @Override
  public int hashCode() { return _id; }


  public String toString() {
    String res = "";
    int tmp = _id + 1;
    do {
      tmp = tmp - 1;
      res = ((char)((tmp % 26) + 97)) + res;
      tmp = tmp / 26;
    } while (tmp != 0);

    return "'" + res;
  }


}

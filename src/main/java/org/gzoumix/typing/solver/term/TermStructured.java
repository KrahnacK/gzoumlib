/**************************************************************************/
/*  Implementation of a simple semi-unification algorithm (Henglein 1993) */
/*  This file is part of libgzoumix                                       */
/*  Copyright (C) 2014. Michael Lienhardt                                 */
/*                                                                        */
/*  This program is free software; you can redistribute it and/or modify  */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation; version 3 of the License.               */
/*                                                                        */
/*  This program is distributed in the hope that it will be useful, but   */
/*  WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     */
/*  General Public License for more details.                              */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with this program; if not, write to the Free Software           */
/*  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA         */
/*  02110-1301 USA                                                        */
/*                                                                        */
/**************************************************************************/

package org.gzoumix.typing.solver.term;

import java.util.*;


public class TermStructured implements ITerm {

  protected Factory _factory;
  protected IConstructor _constructor;
  protected List<ITerm> _subterms;

  TermStructured(Factory factory, IConstructor c, List<ITerm> l) {
    _factory = factory;
    _constructor = c;
    _subterms = l;
  }

  public IConstructor getConstructor() { return _constructor; }
  public List<ITerm> getSubTerms() { return _subterms; }

  /* 1. Term */

  @Override
  public boolean strictlyContains(ITerm t) {
    for(ITerm ts: _subterms) { if (t == ts) return true; }
    for(ITerm ts: _subterms) { if (ts.strictlyContains(t)) return true; }
    return false;
  }

  @Override
  public boolean hasNull() {
    for(ITerm ts: _subterms) {
      if (ts == null) return true;
      else if (ts.hasNull()) return true;
    }
    return false;
  }


  @Override
  public Set<TermVariable> fv() {
    HashSet<TermVariable> res = new HashSet<TermVariable>();
    for(ITerm ts: _subterms) { res.addAll(ts.fv()); }
    return res;
  }

  @Override
  public ITerm subst(TermVariable v, ITerm t) {
    List<ITerm> res = new LinkedList<ITerm>();
    for(ITerm ts: _subterms) { res.add(ts.subst(v, t)); }
    return _factory.createTermStructured(_constructor, res);
  }


  /* 2. Comparable */

  @Override
  public int compareTo(Object t) {
    if(this == t) { return 0; }
    if(t instanceof TermStructured) {
      IConstructor tc = ((TermStructured) t).getConstructor();
      List<ITerm> ts = ((TermStructured) t).getSubTerms();
      int res = this._constructor.compareTo(tc);
      if(res != 0) return res;
      else {
        Iterator<ITerm> ithis = _subterms.iterator();
        Iterator<ITerm> it    = ts.iterator();
        while(ithis.hasNext()) {
          if(!it.hasNext()) return 1;
          res = ithis.next().compareTo(it.next());
          if(res != 0) return res;
        }
        return 0;
      }

    } else return -1;
  }


  /* 3. Object */

  @Override
  public boolean equals(Object t) {
    if (t == this) return true;
    else if ((t!= null) && (t instanceof TermStructured)) {
      TermStructured ts = (TermStructured)t;
      boolean b = (this._constructor.equals(ts._constructor)) && (this._subterms.size() == ts._subterms.size());
      if (b) {
        Iterator<ITerm> i1 = this._subterms.iterator();
        Iterator<ITerm> i2 = ts._subterms.iterator();
        while (i1.hasNext()) {
          if(!i2.hasNext()) return false;
          if (!(i1.next()).equals(i2.next())) return false;
        } return true;
      } else return false;
    } else return false;
  }

  @Override
  public int hashCode() {
    int res = _constructor.hashCode();
    for(ITerm t: _subterms) { res += t.hashCode(); }
    return res;
  }

  @Override
  public String toString() {
    String res = _constructor.toString() + '(';
    Iterator<ITerm> i = this._subterms.iterator();
    while (i.hasNext()) {
      res = res + (i.next().toString());
      if(i.hasNext()) res += ", ";
    }
    res = res + ")";
    return res;
  }
}
